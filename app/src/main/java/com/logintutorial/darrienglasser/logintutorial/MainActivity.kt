package com.logintutorial.darrienglasser.logintutorial

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        login_button.setOnClickListener {
            if (email.text.isEmpty() || password.text.isEmpty()) {
                Toast.makeText(applicationContext,
                    "Email and password must be filled out!",
                    Toast.LENGTH_LONG)
                    .show()
                return@setOnClickListener
            } else {
                if (login_switch.isChecked) {
                    if (f_name.text.isEmpty() || l_name.text.isEmpty()) {
                        Toast.makeText(applicationContext,
                            "First and last name must be filled out to register!",
                            Toast.LENGTH_LONG)
                            .show()
                    } else {
                        Toast.makeText(applicationContext,
                            "Successfully registered!",
                            Toast.LENGTH_LONG)
                            .show()
                    }
                } else {
                    Toast.makeText(applicationContext,
                        "Successfully logged in!",
                        Toast.LENGTH_LONG)
                        .show()

                }
            }
        }
        login_switch.setOnCheckedChangeListener { _, checked ->
            if (checked) {
                switch_text.text = resources.getString(R.string.register)
                login_button.text = resources.getString(R.string.register)
            }

            else {
                switch_text.text = resources.getString(R.string.login)
                login_button.text = resources.getString(R.string.login)
            }
            showRegisterFields(checked)
        }
    }

    private fun showRegisterFields(setShown: Boolean) {
        if (setShown){
            f_name.visibility = View.VISIBLE
            l_name.visibility = View.VISIBLE
        } else {
            f_name.visibility = View.GONE
            l_name.visibility = View.GONE
        }
    }
}
